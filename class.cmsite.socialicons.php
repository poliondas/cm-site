<?php

class CmSiteSocialIcons {


    function __construct() {

    }

    private static function get_types() {
        $aTypes = array(
            'type-1' => array(
                "facebook" => array("0xf09a", "fe-social-icon-facebook"),
                "twitter" => array("0xf302", "fe-social-icon-twitter-1"),
                "instagram" => array("0xf16d", "fe-social-icon-instagram"),
                "pinterest" => array("0xf231", "fe-social-icon-pinterest"),
                "google" => array("0xf05a", "fe-social-icon-gplus-2"),
                "youtube" => array("0xf167", "fe-social-icon-youtube"),
                "vimeo" => array("0xf27d", "fe-social-icon-vimeo"),
                "whatsapp" => array("0xf232", "fe-social-icon-whatsapp"),
            ),
            'type-2' => array(
                "facebook" => array("0xf09a", "fe-social-icon-facebook"),
                "twitter" => array("0x099", "fe-social-icon-twitter"),
                "instagram" => array("0xf32d", "fe-social-icon-instagram-1"),
                "pinterest" => array("0xf231", "fe-social-icon-pinterest"),
                "google" => array("0xf05a", "fe-social-icon-gplus-2"),
                "youtube" => array("0xf167", "fe-social-icon-youtube"),
                "vimeo" => array("0xf27d", "fe-social-icon-vimeo"),
                "whatsapp" => array("0xf232", "fe-social-icon-whatsapp"),
            ),
            'type-3' => array(
                "facebook" => array("0xf308", "fe-social-icon-facebook-squared"),
                "twitter" => array("0xf304", "fe-social-icon-twitter-squared"),
                "instagram" => array("0xf32d", "fe-social-icon-instagram-1"),
                "pinterest" => array("0xf0d3", "fe-social-icon-pinterest-squared"),
                "google" => array("0xf0d4", "fe-social-icon-gplus-squared"),
                "youtube" => array("0xf166", "fe-social-icon-youtube-squared"),
                "vimeo" => array("0xf194", "fe-social-icon-vimeo-squared"),
                "whatsapp" => array("0xf232", "fe-social-icon-whatsapp"),
            ),
            'type-4' => array(
                "facebook" => array("0xf30d", "fe-social-icon-facebook-circled"),
                "twitter" => array("0xf057", "fe-social-icon-twitter-circled-2"),
                "instagram" => array("0xf05e", "fe-social-icon-instagram-circled"),
                "pinterest" => array("0xf0d2", "fe-social-icon-pinterest-circled"),
                "google" => array("0xf059", "fe-social-icon-gplus-circled-1"),
                "youtube" => array("0xf166", "fe-social-icon-youtube-squared"),
                "vimeo" => array("0xf307", "fe-social-icon-vimeo-circled"),
                "whatsapp" => array("0xf232", "fe-social-icon-whatsapp"),
            ),
            'type-5' => array(
                "facebook" => array("0xe800", "fe-social-icon-facebook-circled-1"),
                "twitter" => array("0xe801", "fe-social-icon-twitter-circled-1"),
                "instagram" => array("0xf16d", "fe-social-icon-instagram"),
                "pinterest" => array("0xf16d", "fe-social-icon-pinterest-circled-1"),
                "google" => array("0xf310", "fe-social-icon-gplus-circled"),
                "youtube" => array("0xf166", "fe-social-icon-youtube-squared"),
                "vimeo" => array("0xe802", "fe-social-icon-vimeo-circled-1"),
                "whatsapp" => array("0xf232", "fe-social-icon-whatsapp"),
            ),
        );
        return $aTypes;
    }

    /**
     * Gera os links para as redes sociais
     */
    public static function deploy_icons($style = 'type-1', $size = '', $itens = array()) {
        $aTypes = self::get_types();
        if(!isset($aTypes[$style])){
            return false;
        }
        if(empty($itens)){
            $itens = $aTypes[$style];
        }else{
            $itens = array_flip($itens);
        }
        $classSize = (empty($size)) ? '' : 'fe-social-icon-' . $size;
        echo "<div class='social-icons'>";
        foreach($aTypes[$style] as $rede => $arr){
            if(!isset($itens[$rede])){
                continue;
            }
            $link = get_option('cm_link_' . $rede);
            if(empty($link)){
                continue;
            }
            echo "<a class='btn-social btn-{$rede}' href='{$link}'><i class='fe-social-icon {$classSize} {$arr[2]}'></i></a>";
        }
        echo "</div>";
    }

    public static function html_list()
    {
        $aTypes = self::get_types();
        $html = '';
        foreach($aTypes as $type => $typeArr){
            $htmlItem = '';
            foreach($typeArr as $rede => $typeItem){
                $htmlItem .= "
                    <li title='Code: {$typeItem[0]}'>
                        <i class='fe-social-icon fe-social-icon-2x fe-social-icon-fw {$typeItem[1]}'></i> <span class='i-name'>{$typeItem[1]}</span>
                    </li>
                    ";
            }
            $html .= "
                <ul class='{$type}'>
                    <li>{$type}</li>
                    {$htmlItem}
                </ul>
            ";
        }
        echo "
            <div class='social-icons-types'>
                {$html}
            </div>
        ";
    }
}
