<?php

class CmSite {

    public static function init() {
        /**
         * Register nav menus.
         */
        register_nav_menus(
                array(
                    'main-menu' => 'Menu Principal',
                    'header-menu' => 'Menu do Topo',
                    'footer-menu' => 'Menu do Rodapé',
                    'side-menu' => 'Menu Lateral',
                )
        );

        /*
         * Add post_thumbnails suport.
         */
        add_theme_support('post-thumbnails');

        /**
         * Enables the Excerpt meta box in Page edit screen.
         */
        add_post_type_support('post', 'excerpt');
        add_theme_support('post', 'excerpt');

        /**
         * Support Custom Header.
         */
        /*
          $default = array(
          'width'         => 0,
          'height'        => 0,
          'flex-height'   => false,
          'flex-width'    => false,
          'header-text'   => false,
          'default-image' => '',
          'uploads'       => true,
          );

          add_theme_support( 'custom-header', $default );
         */

        /**
         * Support Custom Editor Style.
         * Allows theme developers to link a custom stylesheet file to the TinyMCE visual editor.
         */
        add_editor_style('assets/css/editor-style.css');

        /**
         * Switch default core markup for search form, comment form, and comments to output valid HTML5.
         */
        add_theme_support(
                'html5', array(
            'search-form',
            'comment-form',
            'comment-list',
            'gallery',
            'caption'
                )
        );

        /*
         * Let WordPress manage the document title.
         * By adding theme support, we declare that this theme does not use a
         * hard-coded <title> tag in the document head, and expect WordPress to
         * provide it for us.
         */
        add_theme_support('title-tag');

        //- Adiciona a variável ajaxurl ao frontend
        add_action('wp_head', 'fixed_ajaxurl');

        function fixed_ajaxurl() {
            $url = admin_url('admin-ajax.php');
            ;
            echo "
          <script type='text/javascript'>
          var ajaxurl = '{$url}';
          </script>
          ";
        }

        //- Cria os metabox
        add_filter('rwmb_meta_boxes', array('CmSite', 'register_metabox'));

        add_action('wp_enqueue_scripts', array('CmSite', 'add_script_custom'));

        /**
         * Adiciona os campos na pagina de configuracoes
         */
        require_once( 'class.cmsite.settings.php' );
    }

    public static function plugin_activation() {
        # code...
    }

    public static function plugin_deactivation() {
        //return self::deactivate_key( self::get_api_key() );
    }

    public static function register_metabox($meta_boxes) {
        $prefix = 'cm_page_';

        $post_id = isset($_GET['post']) ? $_GET['post'] : (isset($_POST['post_ID']) ? $_POST['post_ID'] : false);
        if ($post_id === false) {
            return $meta_boxes;
        }
        $post = get_post($post_id);

        $template_file = get_post_meta($post_id, '_wp_page_template', TRUE);

        if ($template_file) {
            $a_template_file = explode('.', $template_file);
            array_pop($a_template_file);
            $template_name_full = implode('.', $a_template_file);
            $a_template_name = explode('-', $template_name_full);
            array_shift($a_template_name);
            $template_name = implode('-', $a_template_name);
            $template_dir = get_template_directory();
            if (file_exists("{$template_dir}/templates/{$template_name}/metabox.php")) {
                include "{$template_dir}/templates/{$template_name}/metabox.php";
            }
        }

        $meta_boxes[] = array(
            'title' => 'Resumo',
            'post_types' => array('page'),
            'context' => 'advanced',
            'priority' => 'default',
            'autosave' => false,
            'fields' => array(
                array(
                    'id' => $prefix . 'page_resumo_title',
                    'type' => 'text',
                    'name' => 'Titulo do resumo',
                    'post_type' => 'page',
                ),
                array(
                    'id' => $prefix . 'page_resumo',
                    'type' => 'textarea',
                    'name' => 'Resumo da página',
                    'post_type' => 'page',
                    'rows' => 6,
                ),
                // FILE ADVANCED (WP 3.5+)
                array(
                    'name' => 'Foto(img)',
                    'id' => $prefix . "page_resumo_foto",
                    'type' => 'file_advanced',
                    'max_file_uploads' => 1,
                    'mime_type' => 'image', // Leave blank for all file types
                ),
            ),
        );

        return $meta_boxes;
    }

    //- Adiciona js e css


    public static function add_script_custom() {
        $template_uri = get_template_directory_uri();
        $template_dir = get_template_directory();

        wp_enqueue_style('default-style', WP_TEMPLATE_URI . '/assets/css/style.css', array(), null, 'all');
        wp_enqueue_style('grid', WP_TEMPLATE_URI . '/assets/css/grid.css', array(), null, 'all');
        //wp_enqueue_style('fontello', WP_TEMPLATE_URI . '/assets/css/fontello.css', array(), null, 'all');

        wp_deregister_script('jquery');
        wp_enqueue_script('jquery', 'https://code.jquery.com/jquery-2.2.4.min.js', array(), null, true);
        wp_enqueue_script('main', WP_TEMPLATE_URI . '/assets/js/main.js', array('jquery'), null, true);

        global $post;
        $template_file = get_post_meta($post->ID, '_wp_page_template', TRUE);

        if (!empty($template_file)) {
            $a_template_file = explode('.', $template_file);

            array_pop($a_template_file);
            $template_name_full = implode('.', $a_template_file);
            $a_template_name = explode('-', $template_name_full);
            array_shift($a_template_name);
            $template_name = implode('-', $a_template_name);
            if (file_exists("{$template_dir}/templates/{$template_name}/css/front.css")) {
                wp_enqueue_style($template_name, $template_uri . "/templates/{$template_name}/css/front.css", array(), null, 'all');
            }
            if (file_exists("{$template_dir}/templates/{$template_name}/js/front.js")) {
                wp_enqueue_script($template_name, $template_uri . "/templates/{$template_name}/js/front.js", array('main'), null, true);
            }
        }
    }

}
