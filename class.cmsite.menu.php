<?php

class CmSiteMenu {

    function __construct() {

    }

    /**
     * Gera o base64 de um png transparente
     */
    public static function deploy() {
        ?>
        <?php
        if (!empty($title)) {
            echo "<div class='menu-title'>{$title}</div>";
        }
        ?>
        <nav class="nav-outer">
            <div class="btn btn-close"></div>
            <div class='btn btn-menu'>
                <span></span>
                <span></span>
                <span></span>
            </div>
            <div class='nav-inner'>
                <?php
                if (empty($location)) {
                    $location = 'main-menu';
                }
                $navOptions = array(
                    'theme_location' => $location,
                    'depth' => 2,
                    'link_before' => '<span>',
                    'link_after' => '</span>',
                    'menu_class' => 'nav navbar-nav',
                    'container_class' => 'nav-scroll',
                );
                if (is_array($options)) {
                    foreach ($options as $key => $value) {
                        $navOptions[$key] = $value;
                    }
                }

                wp_nav_menu($navOptions);
                ?>
            </div>
        </nav>
        <?php
        }


}
