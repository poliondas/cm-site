<?php
$new_general_setting = new CmSiteSettings();

class CmSiteSettings {

    function __construct() {
        if (is_admin()) {

        }
        add_filter('admin_init', array($this, 'register_company_info_settings'));
        add_filter('admin_menu', array($this, 'register_company_info_submenu_page'));
//add_filter('admin_init', array(&$this, 'register_section'));
//add_filter('admin_init', array(&$this, 'register_fields'));

        add_action('get_header', array($this, 'cm_count_post_views'));
    }

    function register_company_info_submenu_page() {
        add_submenu_page(
                'options-general.php', 'Informações', 'Informações', 'manage_options', 'company-info', array($this, 'register_company_info_submenu_callback'));
    }

    function register_section() {
        add_settings_section('social_links_section', 'Links para redes sociais', array($this, 'add_social_section'), 'company-info', 'social_links_section');
        add_settings_section('contact_section', 'Informacoes para contato', array($this, 'add_contato_section'), 'company-info', 'contact_section');
    }

    function register_company_info_settings() {
        register_setting('company-info', 'cm_link_facebook');
        register_setting('company-info', 'cm_link_instagram');
        register_setting('company-info', 'cm_link_twitter');
        register_setting('company-info', 'cm_link_google');
        register_setting('company-info', 'cm_link_pinterest');
        register_setting('company-info', 'cm_link_youtube');
        register_setting('company-info', 'cm_link_loja_virtual');
        register_setting('company-info', 'cm_link_fashion_film');
        register_setting('company-info', 'cm_info_endereco');
        register_setting('company-info', 'cm_info_telefone');
        register_setting('company-info', 'cm_info_cidade');
        register_setting('company-info', 'cm_info_email');
        register_setting('company-info', 'cm_info_colecao');
        register_setting('company-info', 'cm_info_colecao_ano');
    }

    function register_company_info_submenu_callback() {
        ?>
        <div class="wrap">
            <div id="icon-tools" class="icon32"></div>
            <h2>Preenchas as informações da empresa</h2>

            <form method="post" action="options.php">
                <?php
                settings_fields('company-info');
                do_settings_sections('company-info');
                ?>

                <table class="form-table">
                    <tr valign="top">
                        <th scope="row">Link para o facebook</th>
                        <td><input size="60" type="text" name="cm_link_facebook" value="<?php echo esc_attr(get_option('cm_link_facebook')); ?>" /></td>
                    </tr>

                    <tr valign="top">
                        <th scope="row">Link para o instagram</th>
                        <td><input size="60" type="text" name="cm_link_instagram" value="<?php echo esc_attr(get_option('cm_link_instagram')); ?>" /></td>
                    </tr>

                    <tr valign="top">
                        <th scope="row">Link para o twitter</th>
                        <td><input size="60" type="text" name="cm_link_twitter" value="<?php echo esc_attr(get_option('cm_link_twitter')); ?>" /></td>
                    </tr>

                    <tr valign="top">
                        <th scope="row">Link para o google+</th>
                        <td><input size="60" type="text" name="cm_link_google" value="<?php echo esc_attr(get_option('cm_link_google')); ?>" /></td>
                    </tr>

                    <tr valign="top">
                        <th scope="row">Link para o pinterest</th>
                        <td><input size="60" type="text" name="cm_link_pinterest" value="<?php echo esc_attr(get_option('cm_link_pinterest')); ?>" /></td>
                    </tr>

                    <tr valign="top">
                        <th scope="row">Link para o youtube</th>
                        <td><input size="60" type="text" name="cm_link_youtube" value="<?php echo esc_attr(get_option('cm_link_youtube')); ?>" /></td>
                    </tr>

                    <tr valign="top">
                        <th scope="row">Link para a loja virtual</th>
                        <td><input size="60" type="text" name="cm_link_loja_virtual" value="<?php echo esc_attr(get_option('cm_link_loja_virtual')); ?>" /></td>
                    </tr>

                    <tr valign="top">
                        <th scope="row">Link para o fashion film</th>
                        <td><input size="60" type="text" name="cm_link_fashion_film" value="<?php echo esc_attr(get_option('cm_link_fashion_film')); ?>" /></td>
                    </tr>

                    <tr valign="top">
                        <th scope="row">Coleção Atual</th>
                        <td><input size="60" type="text" name="cm_info_colecao" value="<?php echo esc_attr(get_option('cm_info_colecao')); ?>" /></td>
                    </tr>

                    <tr valign="top">
                        <th scope="row">Ano da Coleção Atual</th>
                        <td><input size="60" type="text" name="cm_info_colecao_ano" value="<?php echo esc_attr(get_option('cm_info_colecao_ano')); ?>" /></td>
                    </tr>

                    <tr valign="top">
                        <th scope="row">Endereço</th>
                        <td><textarea rows="6" name="cm_info_endereco" style="width: 420px; resize: both;"><?php echo esc_attr(get_option('cm_info_endereco')); ?></textarea></td>
                    </tr>

                    <tr valign="top">
                        <th scope="row">Telefone</th>
                        <td><input size="60" type="text" name="cm_info_telefone" value="<?php echo esc_attr(get_option('cm_info_telefone')); ?>" /></td>
                    </tr>

                    <tr valign="top">
                        <th scope="row">E-mail</th>
                        <td><input size="60" type="text" name="cm_info_email" value="<?php echo esc_attr(get_option('cm_info_email')); ?>" /></td>
                    </tr>
                </table>

                <?php submit_button(); ?>
            </form>
        </div>
        <?php
    }

    function register_fields() {
// Facebook
        register_setting('general', 'rede_facebook', 'esc_attr');
        add_settings_field('rede_facebook', '<label for="rede_facebook">' . __('Link para o facebook', 'rede_facebook') . '</label>', array(&$this, 'fields_facebook'), 'general', 'social_links_section');

// Instagram
        register_setting('general', 'rede_instagram', 'esc_attr');
        add_settings_field('rede_instagram', '<label for="rede_instagram">' . __('Link para o instagram', 'rede_instagram') . '</label>', array(&$this, 'fields_instagram'), 'general', 'social_links_section');

// TWitter
        register_setting('general', 'rede_twitter', 'esc_attr');
        add_settings_field('rede_twitter', '<label for="rede_twitter">' . __('Link para o twitter', 'rede_twitter') . '</label>', array(&$this, 'fields_twitter'), 'general', 'social_links_section');

// Google +
        register_setting('general', 'rede_google', 'esc_attr');
        add_settings_field('rede_google', '<label for="rede_google">' . __('Link para o Google+', 'rede_google') . '</label>', array(&$this, 'fields_google'), 'general', 'social_links_section');

// Pinterest
        register_setting('general', 'rede_pinterest', 'esc_attr');
        add_settings_field('rede_pinterest', '<label for="rede_pinterest">' . __('Link para o Pinterest', 'rede_pinterest') . '</label>', array(&$this, 'fields_pinterest'), 'general', 'social_links_section');

// Youtube
        register_setting('general', 'rede_youtube', 'esc_attr');
        add_settings_field('rede_youtube', '<label for="rede_youtube">' . __('Link para perfil do youtube', 'rede_youtube') . '</label>', array(&$this, 'fields_youtube'), 'general', 'social_links_section');

// Fashion film
        register_setting('general', 'fashion_film', 'esc_attr');
        add_settings_field('fashion_film', '<label for="fashion_film">' . __('Link para o fashion film', 'fashion_film') . '</label>', array(&$this, 'fields_fashion_film'), 'general', 'social_links_section');

// Endereço
        register_setting('general', 'empresa_endereco', 'esc_attr');
        add_settings_field('empresa_endereco', '<label for="empresa_endereco">' . __('Endereço da empresa', 'empresa_endereco') . '</label>', array(&$this, 'fields_empresa_endereco'), 'general', 'contact_section');

// Telefone
        register_setting('general', 'empresa_telefone', 'esc_attr');
        add_settings_field('empresa_telefone', '<label for="empresa_telefone">' . __('Telefone principal da empresa', 'empresa_telefone') . '</label>', array(&$this, 'fields_empresa_telefone'), 'general', 'contact_section');

// Cidade
        register_setting('general', 'empresa_cidade', 'esc_attr');
        add_settings_field('empresa_cidade', '<label for="empresa_cidade">' . __('Cidade da empresa', 'empresa_cidade') . '</label>', array(&$this, 'fields_empresa_cidade'), 'general', 'contact_section');

// E-mail contato
        register_setting('general', 'email_contato', 'esc_attr');
        add_settings_field('email_contato', '<label for="email_contato">' . __('E-mail para contato com a empresa', 'email_contato') . '</label>', array(&$this, 'fields_email_contato'), 'general', 'contact_section');
    }

    public function add_social_section($args) {
// echo section intro text here
        echo "<em>informacoes geralmente usadas nos icones do topo ou rodape</em>";
    }

    public function add_contato_section($args) {
// echo section intro text here
        echo "<em>informacoes geralmente usadas no rodape ou pagina de contato</em>";
    }

    function fields_facebook() {
        echo '<input type="text" id="rede_facebook" class="regular-text" name="rede_facebook" value="' . get_option('rede_facebook', '') . '" />';
    }

    function fields_instagram() {
        echo '<input type="text" id="rede_instagram" class="regular-text" name="rede_instagram" value="' . get_option('rede_instagram', '') . '" />';
    }

    function fields_twitter() {
        echo '<input type="text" id="rede_twitter" class="regular-text" name="rede_twitter" value="' . get_option('rede_twitter', '') . '" />';
    }

    function fields_google() {
        echo '<input type="text" id="rede_google" class="regular-text" name="rede_google" value="' . get_option('rede_google', '') . '" />';
    }

    function fields_pinterest() {
        echo '<input type="text" id="rede_pinterest" class="regular-text" name="rede_pinterest" value="' . get_option('rede_pinterest', '') . '" />';
    }

    function fields_youtube() {
        echo '<input type="text" id="rede_youtube" class="regular-text" name="rede_youtube" value="' . get_option('rede_youtube', '') . '" />';
    }

    function fields_fashion_film() {
        echo '<input type="text" id="fashion_film" class="regular-text" name="fashion_film" value="' . get_option('fashion_film', '') . '" />';
    }

    function fields_empresa_endereco() {
        echo '<input type="text" id="empresa_endereco" class="regular-text" name="empresa_endereco" value="' . get_option('empresa_endereco', '') . '" />';
    }

    function fields_empresa_telefone() {
        echo '<input type="text" id="empresa_telefone" class="regular-text" name="empresa_telefone" value="' . get_option('empresa_telefone', '') . '" />';
    }

    function fields_empresa_cidade() {
        echo '<input type="text" id="empresa_cidade" class="regular-text" name="empresa_cidade" value="' . get_option('empresa_cidade', '') . '" />';
    }

    function fields_email_contato() {
        echo '<input type="text" id="email_contato" class="regular-text" name="email_contato" value="' . get_option('email_contato', '') . '" />';
    }

// Conta os views do post
    function cm_count_post_views() {
// Garante que vamos tratar apenas de posts
        if (!is_single()) {
            return;
        } // is_single
// Precisamos da variável $post global para obter o ID do post
        global $post;
        $key = 'cm_post_counter';
// Se a sessão daquele posts não estiver vazia cria a sessão do posts
        if (!empty($_SESSION[$key . '_' . $post->ID])) {
            return;
        } // Checa a sessão
        $_SESSION[$key . '_' . $post->ID] = 1;
// Cria ou obtém o valor da chave para contarmos
        $key_value = (int) get_post_meta($post->ID, $key, true) + 1;
        update_post_meta($post->ID, $key, $key_value);
        return;
    }

}
