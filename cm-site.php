<?php

if (!session_id()) {
    session_start();
}
/**
 * @package CMSite
 */
/*
  Plugin Name: CM Site
  Plugin URI: http://marknet.com.br/
  Description: Base config for sites.
  Version: 1.0.0
  Author: Carlos Eduardo Tormina Mateus
  Author URI: https://www.linkedin.com/in/carlos-mateus-52605a9a/
  License: GPLv2 or later
  Text Domain: site
 */

// Make sure we don't expose any info if called directly
if (!function_exists('add_action')) {
    echo 'Hi there!  I\'m just a plugin, not much I can do when called directly.';
    exit;
}
define('WP_POST_REVISIONS', 3);

$aUrl = explode('/wp-content/', WPMU_PLUGIN_URL);
define('WP_SITEURL', $aUrl[0] . '/');
$actual_link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
$aPath = explode(WP_SITEURL, $actual_link);
$path = ($aPath[1] == '') ? 'home' : $aPath[1];

$pageWp = get_page_by_path($path);
if ($pageWp) {
    $post_id = $pageWp->ID;
} else {
    $post_id = isset($_GET['post']) ? $_GET['post'] : (isset($_POST['post_ID']) ? $_POST['post_ID'] : false);
}

$template_file = get_post_meta($post_id, '_wp_page_template', TRUE);
if ($template_file) {
    define('WP_TEMPLATE_FILE', $template_file);

    $template_name = explode('.', $template_file);
    array_pop($template_name);
    $template_name = implode('.', $template_name);
    $template_name = explode('-', $template_name);
    array_shift($template_name);
    $template_name = implode('-', $template_name);

    define('WP_TEMPLATE_NAME', $template_name);
} else {
    define('WP_TEMPLATE_NAME', FALSE);
    define('WP_TEMPLATE_FILE', FALSE);
}

define('WP_UPLOAD_DIR', WP_CONTENT_DIR . '/uploads');
define('WP_UPLOAD_URL', WP_CONTENT_URL . '/uploads');
define('WP_SITE_NAME', get_bloginfo('name'));
define('WP_TEMPLATE_DIR', get_template_directory());
define('WP_TEMPLATE_URI', get_template_directory_uri());
define('WP_TEMPLATE_URL', WP_TEMPLATE_URI);
define('WP_SITE_URL', site_url());

if (!WP_MEMORY_LIMIT) {
    define('WP_MEMORY_LIMIT', '256M');
}

add_filter('show_admin_bar', function() {
    return false;
});

/**
 * Contantes personalizadas
 */
/**
  WP_TEMPLATE_NAME #nome do template de página apresentado no frontend ou editado no backend
  WP_TEMPLATE_FILE #arquivo do template de página apresentado no frontend ou editado no backend
  /**
 * Constantes nativas do WordPress. Fonte: http://wpengineer.com/2382/wordpress-constants-overview/
 */
/*
  XMLRPC_REQUEST #Will be defined if it's a request over the XML-RPC API.

  ##Paths, dirs and links
  ABSPATH #Absolute path to the WordPress root dir.
  WPINC #Relative path to the /wp-includes/
  WP_LANG_DIR #Absolute path to the folder with language files.
  WP_PLUGIN_DIR #Absolute path to the plugins dir.
  WP_PLUGIN_URL #URL to the plugins dir.
  WP_CONTENT_DIR #Absolute path to thewp-content dir.
  WP_CONTENT_URL #URL to the wp-content dir.
  WP_HOME #Home URL of your WordPress.
  WP_SITEURL #URL to the WordPress root dir.
  WP_TEMP_DIR #Absolute path to a dir, where temporary files can be saved.
  WPMU_PLUGIN_DIR #Absolute path to the must use plugin dir.
  WPMU_PLUGIN_URL #URL to the must use plugin dir.

  ##Database
  WP_ALLOW_REPAIR #Allows you to automatically repair and optimize the database tables via /wp-admin/maint/repair.php.

  Cache and script compressing
  COMPRESS_CSS #(De)activates the compressing of stylesheets.
  COMPRESS_SCRIPTS #(De)activates the compressing of Javascript files.
  CONCATENATE_SCRIPTS #(De)activates the consolidation of Javascript or CSS files before compressing.
  ENFORCE_GZIP #(De)activates gzip output.

  ##Debug
  SAVEQUERIES #(De)activates the saving of database queries in an array ($wpdb->queries).
  WP_DEBUG #(De)activates the debug mode in WordPress.
  WP_DEBUG_DISPLAY #(De)activates the display of errors on the screen.
  WP_DEBUG_LOG #(De)activates the writing of errors to the /wp-content/debug.log file.
 */

register_activation_hook(__FILE__, array('CmSite', 'plugin_activation'));
register_deactivation_hook(__FILE__, array('CmSite', 'plugin_deactivation'));

require_once( WPMU_PLUGIN_DIR . '/cm-site/class.cmsite.php' );
require_once( WPMU_PLUGIN_DIR . '/cm-site/class.cmsite.menu.php' );
require_once( WPMU_PLUGIN_DIR . '/cm-site/class.cmsite.socialicons.php' );
require_once( WPMU_PLUGIN_DIR . '/cm-site/class.cmsite.helpers.php' );

add_action('init', array('CmSite', 'init'));

