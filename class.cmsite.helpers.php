<?php

class CmSiteHelpers {

    function __construct() {

    }

    /**
     * Gera o base64 de um png transparente
     */
    public static function blank_png($w = 100, $h = 100) {
        $img = imagecreatetruecolor($w, $h);
// Prepare alpha channel for transparent background
        $alpha_channel = imagecolorallocatealpha($img, 0, 0, 0, 127);
        imagecolortransparent($img, $alpha_channel);
// Fill image
        imagefill($img, 0, 0, $alpha_channel);
// Save transparency
        imagesavealpha($img, true);

        ob_start();
        imagepng($img);
        return 'data:image/png;base64,' . base64_encode(ob_get_clean());
    }

    /** END blank_png */

    /**
     * Paginação simples
     */
    public static function pagination_funtion($mx) {
        $total = $mx;

        if ($total > 1) {
            if (!$current_page = get_query_var('paged'))
                $current_page = 1;

            $big = 999999999;

            $permalink_structure = get_option('permalink_structure');
            $format = empty($permalink_structure) ? '&page=%#%' : 'page/%#%/';
            echo paginate_links(array(
                'base' => str_replace($big, '%#%', get_pagenum_link($big)),
                'format' => $format,
                'current' => $current_page,
                'total' => $total,
                'mid_size' => 2,
                'type' => 'plain'
            ));
        }
    }

    /** END Paginação */

    /**
     * Parse get variables
     */
    public static function parse_get($url) {
        $get = explode('?', $url);
        if (!isset($get[1])) {
            return array();
        }
        $itens = explode('&', $get[1]);
        $variables = array();
        foreach ($itens as $item) {
            $item_ = explode('=', $item);
            if (isset($item_[1])) {
                $variables[$item_[0]] = $item_[1];
            };
        }

        return $variables;
    }

    /** END parse */
    public static function get_url_img(&$arr, $size = false) {
        $file = $arr['file'];
        $aFile = explode('/', $file);
        $fileName = array_pop($aFile);
        $path = implode('/', $aFile);

        foreach ($arr['sizes'] as $key => &$arrSize) {
            $arrSize['url'] = WP_UPLOAD_URL . '/' . $path . '/' . $arrSize['file'];
        }

        $arr['sizes']['full'] = array(
            'file' => $arr['name'],
            'mime-type' => $arr['sizes']['thumbnail']['mime-type'],
            'url' => isset($arr['full_url']) ? $arr['full_url'] : $arr['url'],
            'path' => $arr['path'],
        );

        if (!isset($arr['sizes']['large'])) {
            $arr['sizes']['large'] = $arr['sizes']['full'];
        }
        if (!isset($arr['sizes']['medium_large'])) {
            $arr['sizes']['medium_large'] = $arr['sizes']['large'];
        }
        if (!isset($arr['sizes']['medium'])) {
            $arr['sizes']['medium'] = $arr['sizes']['medium_large'];
        }
        if (!isset($arr['sizes']['thumbnail'])) {
            $arr['sizes']['thumbnail'] = $arr['sizes']['medium'];
        }
    }

    public static function get_meta_img(&$img, $metas = array()) {
        foreach ($metas as $meta) {
            $img['image_meta'][$meta] = get_post_meta($img['ID'], '_' . $meta, true);
        }
    }

    public static function get_post_day($postItem, $type = 'dd') {
        $day = date('d', strtotime($postItem->post_date));
        switch ($type) {
            case 'd': return (string) $day;
            case 'dd': return str_pad($day, 2);
        }
    }

    public static function get_post_month($postItem, $type = 'mm') {
        $aMes = array('Janeiro', 'Fevereiro', 'Marco', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Ouubrot', 'Novembro', 'Dezembro');
        $mes = date('m', strtotime($postItem->post_date));
        switch ($type) {
            case 'd': return (string) $mes;
            case 'dd': return str_pad($mes, 2);
            case 'm': return substr($aMes[(int) $mes], 3);
            case 'mm': return $aMes[(int) $mes];
            default: return '';
        }
    }

    public static function get_post_year($postItem, $type = 'y') {
        $year = ($type === 'y') ? date('y', strtotime($postItem->post_date)) : date('Y', strtotime($postItem->post_date));
        return $year;
    }

    public static function get_post_categories($postItem, $type = '', $separator = '') {
        $post_categories = wp_get_post_categories($postItem->ID);

        $name_categories = array();
        foreach ($post_categories as $c) {
            $cat = get_category($c);
            $name_categories[] = $cat->name;
        }
        switch ($type) {
            case '': echo implode(', ', $name_categories);
        }
    }

    public static function get_grouped_posts($args, $itensPorGrupo = 1) {
        //$recent = new WP_Query($args);
        $recent = get_posts($args);
        $aPosts = array();
        // Start the Loop.
        $i = 0;
        $j = 0;
        //global $post;
        $itensPorGrupo = (int) $itensPorGrupo;
        if ($itensPorGrupo < 1) {
            $itensPorGrupo = 1;
        }

        foreach ($recent as $postItem) {
            //while ($recent->have_posts()) : $recent->the_post();
            if (!isset($aPosts[$i])) {
                $aPosts[$i] = array();
            }
            $aPosts[$i][$j] = $postItem;
            $j++;
            if ($j >= $itensPorGrupo) {
                $j = 0;
                $i++;
            }

            //endwhile;
        }
        wp_reset_query();
        return $aPosts;
    }

    public static function get_loop_grouped_posts($itensPorGrupo = 1) {
        global $post;
        $aPosts = array();
        $itensPorGrupo = (int) $itensPorGrupo;
        if ($itensPorGrupo < 1) {
            $itensPorGrupo = 1;
        }
        if (have_posts()) :
            // Start the Loop.
            $i = 0;
            $j = 0;
            while (have_posts()) : the_post();
                if (!isset($aPosts[$i])) {
                    $aPosts[$i] = array();
                }
                $aPosts[$i][$j] = $post;
                $j++;
                if ($j >= $itensPorGrupo) {
                    $j = 0;
                    $i++;
                }
            endwhile;
        endif;
        wp_reset_query();
        return $aPosts;
    }

    public static function get_mostview_posts($args) {
        $args['meta_key'] = 'cm_post_counter';
        $args['orderby'] = 'meta_value_num';
        $args['order'] = 'DESC';
        $recent = new WP_Query($args);
        $aPosts = array();
        global $post;
        // Start the Loop.
        $i = 0;
        $j = 0;
        while ($recent->have_posts()) : $recent->the_post();
            if (!isset($aPosts[$i])) {
                $aPosts[$i] = array();
            }
            $aPosts[$i][$j] = $post;
            $j++;
            if ($j === 2) {
                $j = 0;
                $i++;
            }
        endwhile;
        wp_reset_query();
        return $aPosts;
    }

}
